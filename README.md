
# Notes

- Because the download link for Cellranger has an expiration time of 30 min, a new link needs to be acquired [here](https://support.10xgenomics.com/single-cell-gene-expression/software/overview/welcome) for the container to be re-built successfully.

# Pulling Images

## Pulling for Development
The following will pull the most recently built image into an "images" subdirectory of your /work directory, (/work/YOUR_NETID/images). 

```
IMAGEDIR=/work/${USER}/images
mkdir -p $IMAGEDIR
singularity pull --force --dir $IMAGEDIR oras://gitlab-registry.oit.duke.edu/hiv_r25/rstudio-singularity-2022-2023-hiv:latest
```

## Pulling for Production
The following command will pull the image with the tag **TAG_NAME** into the directory for production images (/opt/apps/community/od_chsi_rstudio). You can only do this if you have permissions to write to this directory. You must replace **TAG_NAME** with the commit tag of the image you want to pull:

`singularity pull --force --dir /opt/apps/community/od_chsi_rstudio oras://gitlab-registry.oit.duke.edu/hiv_r25/rstudio-singularity-2022-2023-hiv:TAG_NAME`

If you want to deploy this image to be used by the **CHSI RStudio Workshop** App in DCC Open OnDemand, you should run the following to update the softlink that the App uses. Here again You must replace **TAG_NAME** with the commit tag of the image you want to link to.
```
cd /opt/apps/community/od_chsi_rstudio
ln -sf rstudio-singularity-2022-2023-hiv_TAG_NAME.sif hiv_r25_rstudio.sif
```
